package com.michalrys.f1monitor;

public class SimplePlainTextDisplay implements Observer, NewsDisplay {
    private final String name;
    private StringBuilder news = new StringBuilder();


    public SimplePlainTextDisplay(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {
        news.append(message);
        news.append("\n");
    }

    @Override
    public void showNews() {
        System.out.printf("/---%s display-----------------------------------\n", name);
        System.out.print(news.toString());
        System.out.printf("\\---%s display-----------------------------------\n", name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimplePlainTextDisplay that = (SimplePlainTextDisplay) o;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "SimplePlainTextDisplay{" +
                "name='" + name + '\'' +
                '}';
    }
}