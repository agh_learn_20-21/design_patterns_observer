package com.michalrys.f1monitor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class NewsAgent implements Observable {
    private final String name;
    private List<Observer> observers = new ArrayList<>();
    private String hotNews;
    private int newsId = 0;

    public NewsAgent(String name) {
        this.name = name;
    }

    @Override
    public void addObserver(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(hotNews);
        }
    }

    public void setHotNews(String news) {
        hotNews = String.format("| [%s %03d %s]  %s", name, ++newsId, getCurrentTimeStamp(), news);
        notifyObservers();
    }

    private String getCurrentTimeStamp() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd_hh:mm:ss"));
    }
}