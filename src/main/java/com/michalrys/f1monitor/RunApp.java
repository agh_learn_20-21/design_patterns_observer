package com.michalrys.f1monitor;

public class RunApp {
    public static void main(String[] args) {
        //Desing Pattern = Observer
        System.out.println("F1 MONITOR TASK");

        NewsAgent monako = new NewsAgent("Formula 1: Monako race - HOT NEWS");
        NewsAgent australia = new NewsAgent("Formula 1: Australia race - HOT NEWS");
        NewsAgent germany = new NewsAgent("Formula 1: Germany race - HOT NEWS");

        SimplePlainTextDisplay userA = new SimplePlainTextDisplay("Michal");
        SimplePlainTextDisplay userB = new SimplePlainTextDisplay("Tom");
        SimplePlainTextDisplay userC = new SimplePlainTextDisplay("Ola");

        monako.addObserver(userA);
        monako.addObserver(userB);
        monako.addObserver(userC);

        monako.setHotNews("Race was started."); // 01
        australia.setHotNews("Temperature is above 40."); //01
        australia.setHotNews("We are waiting for Schumacher."); //02
        germany.setHotNews("Hello everybody. Welcome in germany."); //01
        germany.setHotNews("Lets start the race..."); //02

        monako.removeObserver(userA);
        australia.addObserver(userA);

        australia.setHotNews("There are some problems on the track, we are still waiting."); //03
        monako.setHotNews("Niki is leading."); // 02
        monako.setHotNews("Niki lost leading - Arni is leading."); // 03
        australia.setHotNews("It looks like nothing was change. Waiting..."); //04
        monako.setHotNews("Last loop."); // 04
        germany.setHotNews("We are after first loop. There are 39..."); //03

        australia.removeObserver(userA);
        monako.addObserver(userA);
        monako.removeObserver(userC);
        germany.addObserver(userC);

        germany.setHotNews("Loop 2 / 40. Booooring...."); //04
        monako.setHotNews("Niki is leading again."); // 05
        monako.setHotNews("Niki won the race."); // 06
        australia.setHotNews("Oh no... race was canceled."); //05
        germany.setHotNews("It's raining..."); //05

        userA.showNews();
        userB.showNews();
        userC.showNews();
    }
}