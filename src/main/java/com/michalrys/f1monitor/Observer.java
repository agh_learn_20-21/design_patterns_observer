package com.michalrys.f1monitor;

public interface Observer {
    void update(String currentNews);
}
